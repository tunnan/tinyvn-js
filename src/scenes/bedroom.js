import { text, button, goto, spacer } from "../core";
import { addCash, hasEnoughCash, spendCash } from "../helpers";
import { mutable } from "../globals";

export function bedroom() {
  return [
    text('This is the bedroom scene'),
    text(`Hello, ${mutable.persistent.player.name}!`),
    spacer(),
    button('Add cash +100', () => {
      addCash(100);
      goto(bedroom);
    }),
    button('Spend cash -50', () => {
      if (hasEnoughCash(50)) {
        spendCash(50);
        goto(bedroom);
      }
    })
  ];
}