import { text, button, input, goto, spacer } from "../core";
import { bedroom } from "./bedroom";
import { mutable } from "../globals";

export function start() {
  return [
    text('This is the start page'),
    spacer(),
    text('What\s your name?'),
    input('John Doe', e => {
      mutable.persistent.player.name = e.target.value;
    }),
    button('Go to the next scene', () => {
      goto(bedroom);
    })
  ];
}