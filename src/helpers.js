import { mutable, timeOfDay } from "./globals";

// ------------------------------
// Time
// ------------------------------
export function advanceTime(amount = 1) {
  mutable.persistent.time = (mutable.persistent.time + amount) % Object.keys(timeOfDay).length;
}

export function setTime(value) {
  mutable.persistent.time = value;
}

export function formatTime() {
  return upperCaseFirstLetter(Object.keys(timeOfDay)[mutable.persistent.time])
    .split('_').join(' ');
}

// ------------------------------
// Money
// ------------------------------
export function hasEnoughCash(amount) {
  return mutable.persistent.player.cash >= amount;
}

export function addCash(amount) {
  mutable.persistent.player.cash += amount;
}

export function spendCash(amount) {
  mutable.persistent.player.cash -= amount;
}

// ------------------------------
// Misc
// ------------------------------
export function upperCaseFirstLetter(string) {
  return string[0].toUpperCase() + string.slice(1);
}