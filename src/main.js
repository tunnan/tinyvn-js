import { render } from "./render";
import { mutable } from "./globals";
import { goto, register_load_game_event, update_save_game_data, register_new_game_event } from "./core";
import { start } from "./scenes/start";
import { bedroom } from "./scenes/bedroom";

// Main entry
document.addEventListener('DOMContentLoaded', () => {
  // Register the event used to save the persistent data when leaving the page
  window.addEventListener('beforeunload', () => {
    localStorage.setItem('persistent', JSON.stringify(mutable.persistent));
  })

  // Load the local storage when loading up the page
  if (localStorage.hasOwnProperty('persistent')) {
    mutable.persistent = JSON.parse(localStorage.getItem('persistent'));
  }

  update_save_game_data();
  register_load_game_event();
  register_new_game_event();

  // Register the custom event to re-render content
  document.addEventListener('renderContents', () => {
    update_save_game_data();
    render(mutable.scene);
  });

  if (localStorage.hasOwnProperty('persistent')) goto(bedroom);
  else goto(start);
});