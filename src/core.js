import { renderEvent, mutable, persistent_defaults } from "./globals";
import { start } from "./scenes/start";
import { bedroom } from "./scenes/bedroom";

// Create the button component
export function button(value, fn) {
  let element = document.createElement('button');
  element.innerHTML = value;
  element.addEventListener('click', fn);
  return element;
}

// Create the image component
export function image(value) {
  let element = document.createElement('img');
  element.src = value;
  return element;
}

// Create the text component
export function text(value) {
  let element = document.createElement('p');
  element.innerHTML = value;
  return element;
}

// Create the input component
export function input(value, fn) {
  let element = document.createElement('input');
  element.type = 'text';
  element.value = value;
  element.addEventListener('keyup', fn);
  return element;
}

// Create the spacer component
export function spacer() {
  let element = document.createElement('div');
  element.className = 'spacer';
  return element;
}

// Change scene and dispatch the render event
export function goto(scene) {
  mutable.scene = scene;
  document.dispatchEvent(renderEvent);
}

// Register the load game event
export function register_load_game_event() {
  document.getElementById('load-game').addEventListener('change', e => {
    const file = e.target.files[0];
    const reader = new FileReader();
    const blob = new Blob([file], { type: "application/json" });
  
    // Make sure we can only load json-files
    if (file.name.slice(file.name.length - 4) != 'json') return;
  
    reader.addEventListener('load', () => {
      localStorage.clear();
      mutable.persistent = JSON.parse(reader.result);

      // Change this to the scene you want to render
      // when loading a new game
      goto(bedroom);
    });
  
    reader.readAsText(blob);
  });
}

// Update the save game button to always have the current persistent data
export function update_save_game_data() {
  document.getElementById('save-game').setAttribute('href', "data:text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(mutable.persistent)));
}

// Register the event for starting a new game
export function register_new_game_event() {
  document.getElementById('new-game').addEventListener('click', e => {
    e.preventDefault();

    if(confirm('Are you sure you wish to start a new game?')) {
      mutable.persistent = persistent_defaults;
      goto(start);
    }
  });
}