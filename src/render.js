import { formatTime } from "./helpers";
import { mutable } from "./globals";

// Render the sidebar and the scene content
export function render(scene) {
  updateSidebar();
  updateContent(scene);
}

// Update the sidebar data
function updateSidebar() {
  const timeContainer = document.getElementById('time');
  const cashContainer = document.getElementById('cash');

  timeContainer.innerHTML = formatTime();
  cashContainer.innerHTML = mutable.persistent.player.cash;
}

// Update the scene elements
function updateContent(scene) {
  const container = document.getElementById('content');
  
  container.innerHTML = '';
  scene().forEach(element => {
    container.appendChild(element);
  });
}