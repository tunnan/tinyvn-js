// Events
export const renderEvent = new CustomEvent('renderContents');

// Time of day
export const timeOfDay = Object.freeze({
  "early_morning": 0,
  "morning": 1,
  "noon": 2,
  "afternoon": 3,
  "evening": 4,
  "night": 5
});

// Persistent variables will be saved when leaving the page.
// Roll back to these default when starting a new game
export const persistent_defaults = {
  time: 0,
  player: {
    name: 'John Doe',
    cash: 100
  }
}

// Mutable variables
export let mutable = {
  scene: null,
  persistent: persistent_defaults
};