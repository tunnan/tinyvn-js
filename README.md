# tinyvn-js
tinyvn-js is a **tiny visual novel engine** written in JavaScript that aims to be small in size and easy to use. There is no graphical interface, and everything is written purely in code

## Features
* No need for any web server! Just build your project and it can be opened directly in the users browser
* Tiny source! The source files a currently just below 7kB
* Easy to customize! Edit the HTML edit CSS to fit your needs
* Full access to the source code! So that you can change things to how you want them

## Installation
First of all, you need to install [Git](https://git-scm.com/) (or download the files manually from this website).
You will also need [Node.js](https://nodejs.org/en/) to get access to the Node Package Manager (npm) to install a few required dependencies

Create a directory for your project and open it within a terminal and enter the following commands
```
git clone https://gitlab.com/tunnan/tinyvn-js.git .
npm install
```

To begin developing, run the code to allow webpack to watch for file changes

```
npm run build
```

## Guides
Guides will be coming soon, but right now there are a couple of example scenes to get you started

## License
This project is distributed under the [GNU General Public License 3.0](/LICENSE) license