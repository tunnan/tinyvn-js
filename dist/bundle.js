/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/main.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/core.js":
/*!*********************!*\
  !*** ./src/core.js ***!
  \*********************/
/*! exports provided: button, image, text, input, spacer, goto, register_load_game_event, update_save_game_data, register_new_game_event */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"button\", function() { return button; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"image\", function() { return image; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"text\", function() { return text; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"input\", function() { return input; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"spacer\", function() { return spacer; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"goto\", function() { return goto; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"register_load_game_event\", function() { return register_load_game_event; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"update_save_game_data\", function() { return update_save_game_data; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"register_new_game_event\", function() { return register_new_game_event; });\n/* harmony import */ var _globals__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./globals */ \"./src/globals.js\");\n/* harmony import */ var _scenes_start__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./scenes/start */ \"./src/scenes/start.js\");\n/* harmony import */ var _scenes_bedroom__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./scenes/bedroom */ \"./src/scenes/bedroom.js\");\n\r\n\r\n\r\n\r\n// Create the button component\r\nfunction button(value, fn) {\r\n  let element = document.createElement('button');\r\n  element.innerHTML = value;\r\n  element.addEventListener('click', fn);\r\n  return element;\r\n}\r\n\r\n// Create the image component\r\nfunction image(value) {\r\n  let element = document.createElement('img');\r\n  element.src = value;\r\n  return element;\r\n}\r\n\r\n// Create the text component\r\nfunction text(value) {\r\n  let element = document.createElement('p');\r\n  element.innerHTML = value;\r\n  return element;\r\n}\r\n\r\n// Create the input component\r\nfunction input(value, fn) {\r\n  let element = document.createElement('input');\r\n  element.type = 'text';\r\n  element.value = value;\r\n  element.addEventListener('keyup', fn);\r\n  return element;\r\n}\r\n\r\n// Create the spacer component\r\nfunction spacer() {\r\n  let element = document.createElement('div');\r\n  element.className = 'spacer';\r\n  return element;\r\n}\r\n\r\n// Change scene and dispatch the render event\r\nfunction goto(scene) {\r\n  _globals__WEBPACK_IMPORTED_MODULE_0__[\"mutable\"].scene = scene;\r\n  document.dispatchEvent(_globals__WEBPACK_IMPORTED_MODULE_0__[\"renderEvent\"]);\r\n}\r\n\r\n// Register the load game event\r\nfunction register_load_game_event() {\r\n  document.getElementById('load-game').addEventListener('change', e => {\r\n    const file = e.target.files[0];\r\n    const reader = new FileReader();\r\n    const blob = new Blob([file], { type: \"application/json\" });\r\n  \r\n    // Make sure we can only load json-files\r\n    if (file.name.slice(file.name.length - 4) != 'json') return;\r\n  \r\n    reader.addEventListener('load', () => {\r\n      localStorage.clear();\r\n      _globals__WEBPACK_IMPORTED_MODULE_0__[\"mutable\"].persistent = JSON.parse(reader.result);\r\n\r\n      // Change this to the scene you want to render\r\n      // when loading a new game\r\n      goto(_scenes_bedroom__WEBPACK_IMPORTED_MODULE_2__[\"bedroom\"]);\r\n    });\r\n  \r\n    reader.readAsText(blob);\r\n  });\r\n}\r\n\r\n// Update the save game button to always have the current persistent data\r\nfunction update_save_game_data() {\r\n  document.getElementById('save-game').setAttribute('href', \"data:text/json;charset=utf-8,\" + encodeURIComponent(JSON.stringify(_globals__WEBPACK_IMPORTED_MODULE_0__[\"mutable\"].persistent)));\r\n}\r\n\r\n// Register the event for starting a new game\r\nfunction register_new_game_event() {\r\n  document.getElementById('new-game').addEventListener('click', e => {\r\n    e.preventDefault();\r\n\r\n    if(confirm('Are you sure you wish to start a new game?')) {\r\n      _globals__WEBPACK_IMPORTED_MODULE_0__[\"mutable\"].persistent = _globals__WEBPACK_IMPORTED_MODULE_0__[\"persistent_defaults\"];\r\n      goto(_scenes_start__WEBPACK_IMPORTED_MODULE_1__[\"start\"]);\r\n    }\r\n  });\r\n}\n\n//# sourceURL=webpack:///./src/core.js?");

/***/ }),

/***/ "./src/globals.js":
/*!************************!*\
  !*** ./src/globals.js ***!
  \************************/
/*! exports provided: renderEvent, timeOfDay, persistent_defaults, mutable */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"renderEvent\", function() { return renderEvent; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"timeOfDay\", function() { return timeOfDay; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"persistent_defaults\", function() { return persistent_defaults; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"mutable\", function() { return mutable; });\n// Events\r\nconst renderEvent = new CustomEvent('renderContents');\r\n\r\n// Time of day\r\nconst timeOfDay = Object.freeze({\r\n  \"early_morning\": 0,\r\n  \"morning\": 1,\r\n  \"noon\": 2,\r\n  \"afternoon\": 3,\r\n  \"evening\": 4,\r\n  \"night\": 5\r\n});\r\n\r\n// Persistent variables will be saved when leaving the page.\r\n// Roll back to these default when starting a new game\r\nconst persistent_defaults = {\r\n  time: 0,\r\n  player: {\r\n    name: 'John Doe',\r\n    cash: 100\r\n  }\r\n}\r\n\r\n// Mutable variables\r\nlet mutable = {\r\n  scene: null,\r\n  persistent: persistent_defaults\r\n};\n\n//# sourceURL=webpack:///./src/globals.js?");

/***/ }),

/***/ "./src/helpers.js":
/*!************************!*\
  !*** ./src/helpers.js ***!
  \************************/
/*! exports provided: advanceTime, setTime, formatTime, hasEnoughCash, addCash, spendCash, upperCaseFirstLetter */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"advanceTime\", function() { return advanceTime; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"setTime\", function() { return setTime; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"formatTime\", function() { return formatTime; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"hasEnoughCash\", function() { return hasEnoughCash; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"addCash\", function() { return addCash; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"spendCash\", function() { return spendCash; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"upperCaseFirstLetter\", function() { return upperCaseFirstLetter; });\n/* harmony import */ var _globals__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./globals */ \"./src/globals.js\");\n\r\n\r\n// ------------------------------\r\n// Time\r\n// ------------------------------\r\nfunction advanceTime(amount = 1) {\r\n  _globals__WEBPACK_IMPORTED_MODULE_0__[\"mutable\"].persistent.time = (_globals__WEBPACK_IMPORTED_MODULE_0__[\"mutable\"].persistent.time + amount) % Object.keys(_globals__WEBPACK_IMPORTED_MODULE_0__[\"timeOfDay\"]).length;\r\n}\r\n\r\nfunction setTime(value) {\r\n  _globals__WEBPACK_IMPORTED_MODULE_0__[\"mutable\"].persistent.time = value;\r\n}\r\n\r\nfunction formatTime() {\r\n  return upperCaseFirstLetter(Object.keys(_globals__WEBPACK_IMPORTED_MODULE_0__[\"timeOfDay\"])[_globals__WEBPACK_IMPORTED_MODULE_0__[\"mutable\"].persistent.time])\r\n    .split('_').join(' ');\r\n}\r\n\r\n// ------------------------------\r\n// Money\r\n// ------------------------------\r\nfunction hasEnoughCash(amount) {\r\n  return _globals__WEBPACK_IMPORTED_MODULE_0__[\"mutable\"].persistent.player.cash >= amount;\r\n}\r\n\r\nfunction addCash(amount) {\r\n  _globals__WEBPACK_IMPORTED_MODULE_0__[\"mutable\"].persistent.player.cash += amount;\r\n}\r\n\r\nfunction spendCash(amount) {\r\n  _globals__WEBPACK_IMPORTED_MODULE_0__[\"mutable\"].persistent.player.cash -= amount;\r\n}\r\n\r\n// ------------------------------\r\n// Misc\r\n// ------------------------------\r\nfunction upperCaseFirstLetter(string) {\r\n  return string[0].toUpperCase() + string.slice(1);\r\n}\n\n//# sourceURL=webpack:///./src/helpers.js?");

/***/ }),

/***/ "./src/main.js":
/*!*********************!*\
  !*** ./src/main.js ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _render__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./render */ \"./src/render.js\");\n/* harmony import */ var _globals__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./globals */ \"./src/globals.js\");\n/* harmony import */ var _core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./core */ \"./src/core.js\");\n/* harmony import */ var _scenes_start__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./scenes/start */ \"./src/scenes/start.js\");\n/* harmony import */ var _scenes_bedroom__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./scenes/bedroom */ \"./src/scenes/bedroom.js\");\n\r\n\r\n\r\n\r\n\r\n\r\n// Main entry\r\ndocument.addEventListener('DOMContentLoaded', () => {\r\n  // Register the event used to save the persistent data when leaving the page\r\n  window.addEventListener('beforeunload', () => {\r\n    localStorage.setItem('persistent', JSON.stringify(_globals__WEBPACK_IMPORTED_MODULE_1__[\"mutable\"].persistent));\r\n  })\r\n\r\n  // Load the local storage when loading up the page\r\n  if (localStorage.hasOwnProperty('persistent')) {\r\n    _globals__WEBPACK_IMPORTED_MODULE_1__[\"mutable\"].persistent = JSON.parse(localStorage.getItem('persistent'));\r\n  }\r\n\r\n  Object(_core__WEBPACK_IMPORTED_MODULE_2__[\"update_save_game_data\"])();\r\n  Object(_core__WEBPACK_IMPORTED_MODULE_2__[\"register_load_game_event\"])();\r\n  Object(_core__WEBPACK_IMPORTED_MODULE_2__[\"register_new_game_event\"])();\r\n\r\n  // Register the custom event to re-render content\r\n  document.addEventListener('renderContents', () => {\r\n    Object(_core__WEBPACK_IMPORTED_MODULE_2__[\"update_save_game_data\"])();\r\n    Object(_render__WEBPACK_IMPORTED_MODULE_0__[\"render\"])(_globals__WEBPACK_IMPORTED_MODULE_1__[\"mutable\"].scene);\r\n  });\r\n\r\n  if (localStorage.hasOwnProperty('persistent')) Object(_core__WEBPACK_IMPORTED_MODULE_2__[\"goto\"])(_scenes_bedroom__WEBPACK_IMPORTED_MODULE_4__[\"bedroom\"]);\r\n  else Object(_core__WEBPACK_IMPORTED_MODULE_2__[\"goto\"])(_scenes_start__WEBPACK_IMPORTED_MODULE_3__[\"start\"]);\r\n});\n\n//# sourceURL=webpack:///./src/main.js?");

/***/ }),

/***/ "./src/render.js":
/*!***********************!*\
  !*** ./src/render.js ***!
  \***********************/
/*! exports provided: render */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"render\", function() { return render; });\n/* harmony import */ var _helpers__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./helpers */ \"./src/helpers.js\");\n/* harmony import */ var _globals__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./globals */ \"./src/globals.js\");\n\r\n\r\n\r\n// Render the sidebar and the scene content\r\nfunction render(scene) {\r\n  updateSidebar();\r\n  updateContent(scene);\r\n}\r\n\r\n// Update the sidebar data\r\nfunction updateSidebar() {\r\n  const timeContainer = document.getElementById('time');\r\n  const cashContainer = document.getElementById('cash');\r\n\r\n  timeContainer.innerHTML = Object(_helpers__WEBPACK_IMPORTED_MODULE_0__[\"formatTime\"])();\r\n  cashContainer.innerHTML = _globals__WEBPACK_IMPORTED_MODULE_1__[\"mutable\"].persistent.player.cash;\r\n}\r\n\r\n// Update the scene elements\r\nfunction updateContent(scene) {\r\n  const container = document.getElementById('content');\r\n  \r\n  container.innerHTML = '';\r\n  scene().forEach(element => {\r\n    container.appendChild(element);\r\n  });\r\n}\n\n//# sourceURL=webpack:///./src/render.js?");

/***/ }),

/***/ "./src/scenes/bedroom.js":
/*!*******************************!*\
  !*** ./src/scenes/bedroom.js ***!
  \*******************************/
/*! exports provided: bedroom */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"bedroom\", function() { return bedroom; });\n/* harmony import */ var _core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../core */ \"./src/core.js\");\n/* harmony import */ var _helpers__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../helpers */ \"./src/helpers.js\");\n/* harmony import */ var _globals__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../globals */ \"./src/globals.js\");\n\r\n\r\n\r\n\r\nfunction bedroom() {\r\n  return [\r\n    Object(_core__WEBPACK_IMPORTED_MODULE_0__[\"text\"])('This is the bedroom scene'),\r\n    Object(_core__WEBPACK_IMPORTED_MODULE_0__[\"text\"])(`Hello, ${_globals__WEBPACK_IMPORTED_MODULE_2__[\"mutable\"].persistent.player.name}!`),\r\n    Object(_core__WEBPACK_IMPORTED_MODULE_0__[\"spacer\"])(),\r\n    Object(_core__WEBPACK_IMPORTED_MODULE_0__[\"button\"])('Add cash +100', () => {\r\n      Object(_helpers__WEBPACK_IMPORTED_MODULE_1__[\"addCash\"])(100);\r\n      Object(_core__WEBPACK_IMPORTED_MODULE_0__[\"goto\"])(bedroom);\r\n    }),\r\n    Object(_core__WEBPACK_IMPORTED_MODULE_0__[\"button\"])('Spend cash -50', () => {\r\n      if (Object(_helpers__WEBPACK_IMPORTED_MODULE_1__[\"hasEnoughCash\"])(50)) {\r\n        Object(_helpers__WEBPACK_IMPORTED_MODULE_1__[\"spendCash\"])(50);\r\n        Object(_core__WEBPACK_IMPORTED_MODULE_0__[\"goto\"])(bedroom);\r\n      }\r\n    })\r\n  ];\r\n}\n\n//# sourceURL=webpack:///./src/scenes/bedroom.js?");

/***/ }),

/***/ "./src/scenes/start.js":
/*!*****************************!*\
  !*** ./src/scenes/start.js ***!
  \*****************************/
/*! exports provided: start */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"start\", function() { return start; });\n/* harmony import */ var _core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../core */ \"./src/core.js\");\n/* harmony import */ var _bedroom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./bedroom */ \"./src/scenes/bedroom.js\");\n/* harmony import */ var _globals__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../globals */ \"./src/globals.js\");\n\r\n\r\n\r\n\r\nfunction start() {\r\n  return [\r\n    Object(_core__WEBPACK_IMPORTED_MODULE_0__[\"text\"])('This is the start page'),\r\n    Object(_core__WEBPACK_IMPORTED_MODULE_0__[\"spacer\"])(),\r\n    Object(_core__WEBPACK_IMPORTED_MODULE_0__[\"text\"])('What\\s your name?'),\r\n    Object(_core__WEBPACK_IMPORTED_MODULE_0__[\"input\"])('John Doe', e => {\r\n      _globals__WEBPACK_IMPORTED_MODULE_2__[\"mutable\"].persistent.player.name = e.target.value;\r\n    }),\r\n    Object(_core__WEBPACK_IMPORTED_MODULE_0__[\"button\"])('Go to the next scene', () => {\r\n      Object(_core__WEBPACK_IMPORTED_MODULE_0__[\"goto\"])(_bedroom__WEBPACK_IMPORTED_MODULE_1__[\"bedroom\"]);\r\n    })\r\n  ];\r\n}\n\n//# sourceURL=webpack:///./src/scenes/start.js?");

/***/ })

/******/ });